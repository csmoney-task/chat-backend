CREATE TABLE images (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  imageId VARCHAR(21) NOT NULL,
  relativePath VARCHAR(120) NOT NULL,
  extension VARCHAR(5) NOT NULL,
  width INT(11) NOT NULL,
  height INT(11) NOT NULL,
  section ENUM('avatars', 'messages', 'misc') NOT NULL,
  isOriginal TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  KEY section (section, imageId)
) ENGINE=InnoDB DEFAULT CHARSET = utf8;
