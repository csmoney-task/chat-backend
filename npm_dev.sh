#!/usr/bin/env bash

export PORT=8080
export NODE_ENV=development
export MYSQL_DATABASE=csmoney-chat

# manually defined variables:
# export MYSQL_HOST
# export MYSQL_USER
# export MYSQL_PASSWORD
# export SENTRY_DSN (optional)

exec npx nodemon -q --inspect --es-module-specifier-resolution=node --experimental-modules src/index.js
