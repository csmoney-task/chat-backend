FROM node:12-alpine

WORKDIR /csmoney/chat-backend

COPY package*.json ./

RUN npm i --production

COPY ./src ./

CMD [ \
    "node", \
    "--es-module-specifier-resolution=node", \
    "--experimental-modules", \
    "index.js" \
]
