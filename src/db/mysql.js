import util from 'util';
import mysql from 'mysql';

const pool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  waitForConnections: true,
  connectionLimit: 20,
  queueLimit: 0,
});

pool.query = util.promisify(pool.query);
pool.queryRow = (q, p) => pool.query(q, p).then(rows => rows[0]);

export default pool;
