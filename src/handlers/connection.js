import qs from 'querystring';
import { chat as chatManager } from '../managers';

export default async (ws, req) => {
  try {
    const queryString = decodeURI(req.url.substr(req.url.indexOf('?') + 1));
    const rawUsername = qs.parse(queryString).username;

    await chatManager.addClient(rawUsername, ws);
  } catch (e) {
    console.error(e);
  }
};
