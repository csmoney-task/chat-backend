const { PORT } = process.env;

export default () => {
  console.log(`Chat WebSocket server started on port ${PORT}`);
};
