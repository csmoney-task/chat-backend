import WebSocket from 'ws';
import { connection, listening } from './handlers';

const { PORT } = process.env;

const ws = new WebSocket.Server({
  port: PORT,
  clientTracking: true,
});

ws.on('connection', connection);
ws.on('listening', listening);
