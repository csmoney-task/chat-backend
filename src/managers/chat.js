import { mysql } from '../db';

const usernameRegExp = /^\w{4,16}$/;

class ChatManager {
  constructor() {
    this.clientMap = new Map();
  }

  getAvailableUsername(usernameToCheck) {
    // it's safe to use RegExp constructor here
    // because we've already validated username using RegExp above
    // eslint-disable-next-line no-useless-escape
    const usernameCheckRegExp = new RegExp(`^${usernameToCheck}(?: \(\d+\))?$`);

    let sameUsernamesCounter = 0;

    for (const username of this.clientMap.keys()) {
      if (usernameCheckRegExp.test(username)) {
        sameUsernamesCounter++;
      }
    }

    return sameUsernamesCounter === 0
      ? usernameToCheck
      : `${usernameToCheck} (${sameUsernamesCounter})`;
  }

  async addClient(rawUsername, ws) {
    if (!usernameRegExp.test(rawUsername)) {
      await this.sendMessage(ws, 'error', {
        status: 400,
        message: 'Invalid username',
      });

      ws.close();

      return;
    }

    const username = this.getAvailableUsername(rawUsername);

    const messages = await this.getChatMessages();

    await this.sendMessage(ws, 'welcome', {
      messages,
      username,
    });

    this.clientMap.set(username, ws);

    /* eslint-disable no-param-reassign */
    ws.onclose = () => this.deleteClient(username);
    ws.onerror = e => console.error(e) || this.deleteClient(username);
    ws.onmessage = message => this.handleMessage(username, message.data);
    /* eslint-enable no-param-reassign */
  }

  deleteClient(username) {
    this.clientMap.delete(username);
  }

  async getChatMessages() {
    const rawMessages = await mysql.query('SELECT * FROM chat_messages');

    return rawMessages.map(message => ({
      attachment: message.attachment,
      username: message.username,
      message: message.text,
    }));
  }

  handleMessage(fromUsername, rawMessage) {
    try {
      const message = JSON.parse(rawMessage);

      if (message.type === 'message') {
        this.broadcast(fromUsername, message.data);
      } else {
        console.warn(`Unhandled chat message from ${fromUsername}`, message);
      }
    } catch (e) {
      console.error(e);
    }
  }

  async broadcast(fromUsername, messageData) {
    const { message, attachment } = messageData;

    if (message === undefined || message.length === 0) {
      await this.sendMessage(this.clientMap.get(fromUsername), 'error', {
        status: 400,
        message: 'Invalid message text',
      });

      return;
    }

    await mysql.query('INSERT INTO chat_messages SET ?', {
      username: fromUsername,
      text: message,
      attachment,
    });

    const sendMessagePromises = [];

    for (const ws of this.clientMap.values()) {
      sendMessagePromises.push(this.sendMessage(ws, 'message', {
        username: fromUsername,
        attachment,
        message,
      }));
    }

    await Promise.all(sendMessagePromises);
  }

  sendMessage(ws, type, data) {
    const payload = JSON.stringify({
      type,
      data,
    });

    return new Promise((resolve, reject) => ws.send(payload, (e) => {
      if (e) reject(e);
      else resolve();
    }));
  }
}

export default new ChatManager();
